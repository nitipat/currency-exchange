//
//  ViewController.m
//  Ass1V2
//
//  Created by SvZ on 8/31/13.
//  Copyright (c) 2013 SvZ. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@interface Currency : NSObject{
    NSString *cid;
    NSString *name;
    NSString *to;
    NSString *from;
    NSString *fullname;
    NSString *used;

}
@property(nonatomic,copy) NSString *cid;
@property(nonatomic,copy) NSString *name;
@property(nonatomic,copy) NSString *to;
@property(nonatomic,copy) NSString *from;
@property(nonatomic,copy) NSString *fullname;
@property(nonatomic,copy) NSString *used;
@end

@implementation Currency
@synthesize cid;
@synthesize name;
@synthesize to;
@synthesize from;
@synthesize fullname;
@synthesize used;
@end



@implementation ViewController

- (void)viewDidLoad
{
    [self initDatabase];
    //[self databaseQuery:@"delete from currency_choice"];
    //[self getCurrencyFromDB];
    [super viewDidLoad];
    
    Picker.delegate = self;
    Picker.dataSource = self;
    
    ToInput.delegate = self;
    FromInput.delegate = self; 
    
   // [self databaseQuery:@"delete * from currency_choice"];
    //[self databaseQuery:@"insert into currency_choice (name) VALUES ('EUR'), ('GBP')"];
    
    
    //NSLog(@"%@", [self getMoney: 100 :@"USD": @"THB"]);
    
	// Do any additional setup after loading the view, typically from a nib.
}
-(void)viewWillAppear:(BOOL)animated
{
    [self getCurrencyFromDB];
    [Picker reloadAllComponents];
    if([ListFrom count] > 0) {
        [Picker selectRow:0 inComponent:0 animated:YES];
        [Picker selectRow:0 inComponent:1 animated:YES];
        [self setCurrencyLabel:0 :0];
    }
}
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    //Two columns
    return 2;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    //set number of rows
    if(component== 0)
    {
        return [ListFrom count];
    }
    else
    {
        return [ListTo count];
    }
}
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row
            forComponent:(NSInteger)component
{
    if (component == 0) {
        return ((Currency*)[ListFrom  objectAtIndex:row]).name;
        
    }
    return ((Currency*)[ListTo objectAtIndex:row]).name;
    
}
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    firstComponentRow = [Picker selectedRowInComponent:0];
    secondComponentRow = [Picker selectedRowInComponent:1];

    //FirstImg.image = [UIImage imageNamed:[NSString stringWithFormat:@"../flag/%@.gif", [ListName objectAtIndex:firstComponentRow]]];
    
    [self setCurrencyLabel:firstComponentRow :secondComponentRow];
    
}
- (void)setCurrencyLabel: (int)First :(int)Second {
    /*
    NSURL *imageURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://exchangerateweb.com/img/flag/s/%@.gif", ((Currency*)[ListFrom objectAtIndex:First]).name]];
    NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
    FirstImg.image = [UIImage imageWithData:imageData];*/
    FirstImg.image = [UIImage imageNamed:[NSString stringWithFormat:@"../flag/%@.gif", ((Currency*)[ListFrom objectAtIndex:First]).name]];
    FromInput.text = ((Currency*)[ListFrom objectAtIndex:First]).fullname;
    /*
    imageURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://exchangerateweb.com/img/flag/s/%@.gif", ((Currency*)[ListTo objectAtIndex:Second]).name]];
    imageData = [NSData dataWithContentsOfURL:imageURL];
    SecondImg.image = [UIImage imageWithData:imageData];*/
    SecondImg.image = [UIImage imageNamed:[NSString stringWithFormat:@"../flag/%@.gif", ((Currency*)[ListTo objectAtIndex:Second]).name]];
    ToInput.text = ((Currency*)[ListTo objectAtIndex:Second]).fullname;
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    if (textField == FromInput || textField == ToInput)
    {
        //[self openPicker];
        return NO;
    }
    return YES;
}
- (NSString*)getMoney: (NSInteger)Amount: (NSString*)From: (NSString*)To {
    NSURL *myURL =  [NSURL URLWithString: [NSString stringWithFormat:@"http://www.google.com/ig/calculator?q=%d%@=?%@", Amount, From, To]];
    NSLog(@"%@", myURL);
    NSData *data = [NSData alloc];
    data=[NSData dataWithContentsOfURL:myURL];
    NSTextCheckingResult *match;
    NSString *searchedString = nil;
    UIAlertView *loading = [[UIAlertView alloc] initWithTitle:@"" message:@"Please Wait..." delegate:nil cancelButtonTitle:nil otherButtonTitles:nil];
    UIActivityIndicatorView *progress= [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(125, 50, 30, 30)];
    progress.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
    [loading addSubview:progress];
    [progress startAnimating];
    [progress release];
    [loading show];
    if(data != nil){
        [loading dismissWithClickedButtonIndex:0 animated:YES];
        [progress release];
        NSString *response = [[NSString alloc] initWithData:data encoding:NSISOLatin1StringEncoding];
        NSString *pattern = @"lhs:[^\"]?\"([^\"]+)\",rhs:[^\"]?\"([^\"]+)\"";
        NSError *error = nil;
        searchedString = response;
        NSRegularExpression* regex = [NSRegularExpression regularExpressionWithPattern:pattern options:0 error:&error];
        match = [regex firstMatchInString:searchedString options:0 range:NSMakeRange(0, [searchedString length])];
        //NSLog(@"group1: %@", [searchedString substringWithRange:[match rangeAtIndex:1]]);
        //NSLog(@"group2: %@", [searchedString substringWithRange:[match rangeAtIndex:2]]);
        //NSLog(@"%@", response);
        //NSLog(@"%@",matches indexOfObject:1);
    }
    return [searchedString substringWithRange:[match rangeAtIndex:2]];
}
- (void) initDatabase
{
	BOOL success;
	NSFileManager *fileManager = [NSFileManager defaultManager];
	NSError *error;
	NSArray *paths = NSSearchPathForDirectoriesInDomains
    (NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	NSString *writableDBPath = [documentsDirectory stringByAppendingPathComponent:DBFILE];
	success = [fileManager fileExistsAtPath:writableDBPath];
	if (success)
	{
		return;
	}
	NSString *defaultDBPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:DBFILE];
	success = [fileManager copyItemAtPath:defaultDBPath toPath:writableDBPath error:&error];
	if (!success)
	{
        NSLog(@"DB fail");
    }
}
- (void) getCurrencyFromDB
{
    ListFrom = [[NSMutableArray alloc] init];
    ListTo = [[NSMutableArray alloc] init];
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	NSString *path = [documentsDirectory stringByAppendingPathComponent:DBFILE];
    NSLog(@"%@", path);
	if (sqlite3_open([path UTF8String], &database) == SQLITE_OK)
	{
		const char *sql = "select * from currency_choice WHERE used = 1 order by num_from DESC";
		sqlite3_stmt *searchStatement;
		if (sqlite3_prepare_v2(database, sql, -1, &searchStatement, NULL) == SQLITE_OK)
		{
			while (sqlite3_step(searchStatement) == SQLITE_ROW)
			{
                Currency *cur = [[Currency alloc] init];
                cur.cid = [NSString stringWithUTF8String:(char *)sqlite3_column_text(searchStatement, 0)];
                cur.name = [NSString stringWithUTF8String:(char *)sqlite3_column_text(searchStatement, 1)];
                cur.from = [NSString stringWithUTF8String:(char *)sqlite3_column_text(searchStatement, 2)];
                cur.to = [NSString stringWithUTF8String:(char *)sqlite3_column_text(searchStatement, 3)];
                cur.fullname = [NSString stringWithUTF8String:(char *)sqlite3_column_text(searchStatement, 4)];
                [ListFrom addObject:cur];
			}
		}
		sqlite3_finalize(searchStatement);
        sql = "select * from currency_choice WHERE used = 1 order by num_to DESC";
		if (sqlite3_prepare_v2(database, sql, -1, &searchStatement, NULL) == SQLITE_OK)
		{
			while (sqlite3_step(searchStatement) == SQLITE_ROW)
			{
                Currency *cur = [[Currency alloc] init];
                cur.cid = [NSString stringWithUTF8String:(char *)sqlite3_column_text(searchStatement, 0)];
                cur.name = [NSString stringWithUTF8String:(char *)sqlite3_column_text(searchStatement, 1)];
                cur.from = [NSString stringWithUTF8String:(char *)sqlite3_column_text(searchStatement, 2)];
                cur.to = [NSString stringWithUTF8String:(char *)sqlite3_column_text(searchStatement, 3)];
                cur.fullname = [NSString stringWithUTF8String:(char *)sqlite3_column_text(searchStatement, 4)];
                [ListTo addObject:cur];
			}
		}
		sqlite3_finalize(searchStatement);
        sqlite3_close(database);
        //NSLog(@"%@", ((Currency*)[TheCurrencys objectAtIndex:0]).name);
	}
}
- (BOOL) databaseQuery:(NSString*) query
{
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	NSString *path = [documentsDirectory stringByAppendingPathComponent:DBFILE];
	if (sqlite3_open([path UTF8String], &database) == SQLITE_OK)
	{
        const char *sql = [query UTF8String];
        if (sqlite3_exec(database, sql, NULL, NULL, NULL) != SQLITE_OK)
        {
            
        }
        sqlite3_close(database);
        return true;
	} else {
        return false;
    }
}
- (IBAction)Convert:(id)sender {
    [self databaseQuery:[NSString stringWithFormat:@"update currency_choice set num_from = num_from + 1 WHERE name = '%@'", ((Currency*)[ListFrom objectAtIndex:firstComponentRow]).name]];
    [self databaseQuery:[NSString stringWithFormat:@"update currency_choice set num_to = num_to + 1 WHERE name = '%@'", ((Currency*)[ListTo objectAtIndex:secondComponentRow]).name]];
    NSString *out = @"";
    out = [out stringByAppendingFormat:@"%@", [self getMoney: AmountInput.text.integerValue :((Currency*)[ListFrom objectAtIndex:firstComponentRow]).name: ((Currency*)[ListTo objectAtIndex:secondComponentRow]).name]];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Result" message:out delegate:self cancelButtonTitle:@"Try again" otherButtonTitles:nil];
    
    [alert show];
    [self.view endEditing:YES];
    
}
- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}
@end
