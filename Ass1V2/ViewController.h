//
//  ViewController.h
//  Ass1V2
//
//  Created by SvZ on 8/31/13.
//  Copyright (c) 2013 SvZ. All rights reserved.
//

#define DBFILE @"db_v2.sqlite"

#import <UIKit/UIKit.h>
#import <sqlite3.h>

@interface ViewController : UIViewController<UIPickerViewDelegate, UIPickerViewDataSource> {
    NSMutableArray *TheCurrencys;
    NSMutableArray *TheCurrencys2;
    NSMutableArray *ListFrom;
    NSMutableArray *ListTo;
    sqlite3 *database;
    NSInteger firstComponentRow;
    NSInteger secondComponentRow;
    __weak IBOutlet UITextField *FromInput;
    __weak IBOutlet UITextField *ToInput;
    __weak IBOutlet UIPickerView *Picker;
    __weak IBOutlet UITextField *AmountInput;
    __weak IBOutlet UIImageView *FirstImg;
    __weak IBOutlet UIImageView *SecondImg;
}

- (IBAction)Convert:(id)sender;

@end
