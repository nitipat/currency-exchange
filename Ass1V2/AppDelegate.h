//
//  AppDelegate.h
//  Ass1V2
//
//  Created by SvZ on 8/31/13.
//  Copyright (c) 2013 SvZ. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
