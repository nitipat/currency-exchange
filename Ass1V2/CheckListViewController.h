//
//  CheckListViewController.h
//  Ass1V2
//
//  Created by SvZ on 9/5/13.
//  Copyright (c) 2013 SvZ. All rights reserved.
//
#define DBFILE @"db_v2.sqlite"

#import <UIKit/UIKit.h>
#import <sqlite3.h>

@interface CheckListViewController : UITableViewController
{
    NSInteger CheckedNum;
    NSMutableArray *TheCurrency;
    sqlite3 *database;
    IBOutlet UITableView *TableList;
}

@property (nonatomic, strong) NSMutableArray *objects;
@property (nonatomic, strong) NSMutableArray *results;

@end
