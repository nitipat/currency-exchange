//
//  CheckListViewController.m
//  Ass1V2
//
//  Created by SvZ on 9/5/13.
//  Copyright (c) 2013 SvZ. All rights reserved.
//

#import "CheckListViewController.h"

@interface CheckListViewController ()

@end

@interface Currency : NSObject{
    NSString *cid;
    NSString *name;
    NSString *to;
    NSString *from;
    NSString *fullname;
    NSString *used;
    
}
@property(nonatomic,copy) NSString *cid;
@property(nonatomic,copy) NSString *name;
@property(nonatomic,copy) NSString *to;
@property(nonatomic,copy) NSString *from;
@property(nonatomic,copy) NSString *fullname;
@property(nonatomic,copy) NSString *used;
@end

@implementation CheckListViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    TableList.delegate = self;
    TableList.dataSource = self;
    
    self.tableView.backgroundColor = [UIColor clearColor];
    self.view.backgroundColor = [UIColor colorWithPatternImage: [UIImage imageNamed:@"bg-select.png"]];
    self.tableView.opaque = NO;
    
    //self.tableView.backgroundColor = [UIColor colorWithRed:144 green:188 blue:50 alpha:1];
    
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}
-(void)viewWillAppear:(BOOL)animated
{
    [self getCurrencyList];
    [TableList reloadData];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [TheCurrency count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    cell.imageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"../flag/%@.gif", ((Currency*)[TheCurrency objectAtIndex:indexPath.row]).name]];
    
    UIFont *cellFont = [UIFont fontWithName:@"SukhumvitSet.ttc" size:9];
    cell.textLabel.font = cellFont;
    cell.textLabel.text = ((Currency*)[TheCurrency objectAtIndex:indexPath.row]).name;
    //cell.detailTextLabel.textColor = [UIColor colorWithRed:62 green:62 blue:62 alpha:1];
    cell.detailTextLabel.font = cellFont;
    cell.detailTextLabel.text = ((Currency*)[TheCurrency objectAtIndex:indexPath.row]).fullname;
    if ([((Currency*)[TheCurrency objectAtIndex:indexPath.row]).used isEqualToString: @"1"]) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    } else {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     */
    
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:
                             indexPath];
    
    NSLog(@"%d", CheckedNum);
    if (cell.accessoryType == UITableViewCellAccessoryCheckmark) {
        if (CheckedNum != 1) {
            NSString *query = [NSString stringWithFormat:@"update currency_choice set used = 0 WHERE name = '%@'", ((Currency*)[TheCurrency objectAtIndex:indexPath.row]).name];
            [self databaseQuery:query];
            cell.accessoryType = UITableViewCellAccessoryNone;
            CheckedNum--;
            
        }
    } else {
        NSString *query = [NSString stringWithFormat:@"update currency_choice set used = 1 WHERE name = '%@'", ((Currency*)[TheCurrency objectAtIndex:indexPath.row]).name];
        [self databaseQuery:query];
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
        CheckedNum++;
    }
    //NSLog(@"%@", ((Currency*)[TheCurrency objectAtIndex:indexPath.row]).name);
     [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
}
- (void)initDatabase
{
	BOOL success;
	NSFileManager *fileManager = [NSFileManager defaultManager];
	NSError *error;
	NSArray *paths = NSSearchPathForDirectoriesInDomains
    (NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	NSString *writableDBPath = [documentsDirectory stringByAppendingPathComponent:DBFILE];
	success = [fileManager fileExistsAtPath:writableDBPath];
	if (success)
	{
		return;
	}
	NSString *defaultDBPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:DBFILE];
	success = [fileManager copyItemAtPath:defaultDBPath toPath:writableDBPath error:&error];
	if (!success)
	{
        NSLog(@"DB fail");
    }
}
- (void)getCurrencyList
{
    CheckedNum = 0;
    TheCurrency = [[NSMutableArray alloc] init];
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	NSString *path = [documentsDirectory stringByAppendingPathComponent:DBFILE];
    //NSLog(@"%@", path);
	if (sqlite3_open([path UTF8String], &database) == SQLITE_OK)
	{
        const char *sql = "select * from currency_choice order by name ASC";
		sqlite3_stmt *searchStatement;
		if (sqlite3_prepare_v2(database, sql, -1, &searchStatement, NULL) == SQLITE_OK)
		{
			while (sqlite3_step(searchStatement) == SQLITE_ROW)
			{
                Currency *cur = [[Currency alloc] init];
                cur.cid = [NSString stringWithUTF8String:(char *)sqlite3_column_text(searchStatement, 0)];
                cur.name = [NSString stringWithUTF8String:(char *)sqlite3_column_text(searchStatement, 1)];
                cur.from = [NSString stringWithUTF8String:(char *)sqlite3_column_text(searchStatement, 2)];
                cur.to = [NSString stringWithUTF8String:(char *)sqlite3_column_text(searchStatement, 3)];
                cur.fullname = [NSString stringWithUTF8String:(char *)sqlite3_column_text(searchStatement, 4)];
                cur.used = [NSString stringWithUTF8String:(char *)sqlite3_column_text(searchStatement, 5)];
                if ([cur.used isEqualToString:@"1"]){
                    CheckedNum++;
                }
                [TheCurrency addObject:cur];
			}
		}
		sqlite3_finalize(searchStatement);
        sqlite3_close(database);
        //NSLog(@"%d", CheckedNum);
        //NSLog(@"%@", ((Currency*)[TheCurrencys objectAtIndex:0]).name);
	}
}
- (BOOL) databaseQuery:(NSString*) query
{
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	NSString *path = [documentsDirectory stringByAppendingPathComponent:DBFILE];
	if (sqlite3_open([path UTF8String], &database) == SQLITE_OK)
	{
        const char *sql = [query UTF8String];
        if (sqlite3_exec(database, sql, NULL, NULL, NULL) != SQLITE_OK)
        {
            
        }
        sqlite3_close(database);
        return true;
	} else {
        return false;
    }
}
@end
