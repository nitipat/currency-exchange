//
//  main.m
//  Ass1V2
//
//  Created by SvZ on 8/31/13.
//  Copyright (c) 2013 SvZ. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
